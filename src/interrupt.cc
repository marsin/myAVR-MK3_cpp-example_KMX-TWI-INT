/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt.
///
/// @file interrupt.cc
/// @author Martin Singer


#include <avr/interrupt.h>
#include "interrupt.h"


volatile bool Flag_NewKeyEventAvailable = false;
volatile bool Flag_DebounceTimerTimeUp = false;
volatile bool Flag_PrintTimerTimeUp = false;


/** ISR: External TWI Interrupt.
 *
 * The interrupt routine is called from the TWI port reader IC.
 *
 * This routine sets the flag that indicates the system
 * there was an event to poll the keys for detecting which
 * key had the condition change.
 *
 * This routine is used if the INTERRUPT mode is active.
 */
ISR(INT2_vect)
{
	Flag_NewKeyEventAvailable = true;
}


/** ISR: Polling Timer.
 *
 * Timer0 is initialized to call this routine in 1KHz iterations.
 *
 * This routine sets the flag that indicates the system
 * it is time to poll the keys for detecting condition changes.
 *
 * This routine is used if the INTERRUPT mode is not active.
 */
ISR(TIMER0_COMPA_vect)
{
	Flag_DebounceTimerTimeUp = true;
}


ISR(TIMER3_COMPA_vect)
{
	Flag_PrintTimerTimeUp = true;
}



/*
 * ## Timer/Counter 0 Registers
 *
 * Source: ATmega2560 (2549L–AVR–08/07) Page 129, 170
 *
 * - TCCR0A - Timer/Counter 0: Control Register A
 *   - Bit 7 - COM0A1: Compare Match Output A Mode (r/w)
 *   - Bit 6 - COM0A0:                             (r/w)
 *   - Bit 5 - COM0B1: Compare Match Output B Mode (r/w)
 *   - Bit 4 - COM0B0:                             (r/w)
 *   - Bit 3 - reserved
 *   - Bit 2 - reserved
 *   - Bit 1 - WGM01:  Waveform Generation Mode    (r/w)
 *   - Bit 0 - WGM00:                              (r/w)
 *
 * - TCCR0B – Timer/Counter 0: Control Register B
 *   - Bit 7 – FOC0A: Force Output Compare A       (r/w)
 *   - Bit 6 – FOC0B: Force Output Compare B       (r/w)
 *   - Bit 5 – reserved                            (r)
 *   - Bit 4 – reserved                            (r)
 *   - Bit 3 – WGM02: Waveform Generation Mode     (r/w)
 *   - Bit 2 – CS02:  Clock Select                 (r/w)
 *   - Bit 1 – CS01:                               (r/w)
 *   - Bit 0 – CS00:                               (r/w)
 *
 * - TCNT0 – Timer/Counter 0: Register
 *   - TCNT0[7:0] (r/w)
 *
 * - OCR0A – Timer/Counter 0: Output Compare Register A
 *   - OCR0A[7:0] (r/w)
 *
 * - OCR0B – Timer/Counter 0: Output Compare Register B
 *   - OCR0B[7:0] (r/w)
 *
 * - TIMSK0 – Timer/Counter 0: Interrupt Mask Register
 *   - Bit 7 - reserved                                                       (r)
 *   - Bit 6 - reserved                                                       (r)
 *   - Bit 5 - reserved                                                       (r)
 *   - Bit 4 - reserved                                                       (r)
 *   - Bit 3 - reserved                                                       (r)
 *   - Bit 2 – OCIE0B: Timer/Counter0 Output Compare Match B Interrupt Enable (r/w)
 *   - Bit 1 – OCIE0A: Timer/Counter0 Output Compare Match A Interrupt Enable (r/w)
 *   - Bit 0 – TOIE0:  Timer/Counter0 Overflow Interrupt Enable               (r/w)
 *
 * - TIFR0 – Timer/Counter 0: Interrupt Flag Register
 *   - Bit 7 - reserved                                                       (r)
 *   - Bit 6 - reserved                                                       (r)
 *   - Bit 5 - reserved                                                       (r)
 *   - Bit 4 - reserved                                                       (r)
 *   - Bit 3 - reserved                                                       (r)
 *   - Bit 2 – OCF0B: Timer/Counter0 Output Compare B Match Flag              (r/w)
 *   - Bit 1 – OCF0A: Timer/Counter0 Output Compare A Match Flag              (r/w)
 *   - Bit 0 – TOV0:  Timer/Counter0 Overflow Flag                            (r/w)
 */
/*
 * ## Timer Modes (Register: TCCR0A and TCCR0B)
 *
 * Source: ATmega2560 (2549L–AVR–08/07) Page 129
 *
 * ### Compare Output Mode, non PWM Mode
 *
 * COM0A1 | COM0A0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0A disconnected.
 * 0      | 1      | Toggle OC0A on Compare Match
 * 1      | 0      | Clear  OC0A on Compare Match
 * 1      | 1      | Set    OC0A on Compare Match
 *
 *
 * ### Compare Output Mode, Fast PWM Mode
 *
 * COM0A1 | COM0A0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0A disconnected.
 * 0      | 1      | WGM02 = 0: Normal Port Operation, OC0A Disconnected.
 *        |        | WGM02 = 1: Toggle OC0A on Compare Match.
 * 1      | 0      | Clear OC0A on Compare Match, set OC0A at BOTTOM,
 *        |        | (non-inverting mode).
 * 1      | 1      | Set OC0A on Compare Match, clear OC0A at BOTTOM,
 *        |        | (inverting mode).
 *
 *
 * ### Compare Output Mode, Phase Correct PWM Mode
 *
 * COM0A1 | COM0A0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0A disconnected.
 * 0      | 1      | WGM02 = 0: Normal Port Operation, OC0A Disconnected.
 *        |        | WGM02 = 1: Toggle OC0A on Compare Match.
 * 1      | 0      | Clear OC0A on Compare Match when up-counting. Set OC0A on
 *        |        | Compare Match when down-counting.
 * 1      | 1      | Set OC0A on Compare Match when up-counting. Clear OC0A on
 *        |        | Compare Match when down-counting.
 *
 *
 * ### Compare Output Mode, non-PWM Mode
 *
 * COM0B1 | COM0B0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0B disconnected.
 * 0      | 1      | Toggle OC0B on Compare Match
 * 1      | 0      | Clear OC0B on Compare Match
 * 1      | 1      | Set OC0B on Compare Match
 *
 *
 * ### Compare Output Mode, Fast PWM Mode
 *
 * COM0B1 | COM0B0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0B disconnected.
 * 0      | 1      | Reserved
 * 1      | 0      | Clear OC0B on Compare Match, set OC0B at BOTTOM,
 *        |        | (non-inverting mode).
 * 1      |        | 1 Set OC0B on Compare Match, clear OC0B at BOTTOM,
 *        |        | (inverting mode).
 *
 *
 * ### Compare Output Mode, Phase Correct PWM Mode
 *
 * COM0B1 | COM0B0 | Description
 * ------ | ------ | -----------
 * 0      | 0      | Normal port operation, OC0B disconnected.
 * 0      | 1      | Reserved
 * 1      | 0      | Clear OC0B on Compare Match when up-counting. Set OC0B on
 *        |        | Compare Match when down-counting.
 * 1      | 1      | Set OC0B on Compare Match when up-counting. Clear OC0B on
 *        |        | Compare Match when down-counting.
 *
 *
 * ### Waveform Generation Mode Bit Description
 *
 * Mode | WGM2 | WGM1 | WGM0 | Timer/Counter Mode | TOP  | Update of OCRx | TOV Flag Set
 * ---- | ---- | ---- | ---- | ------------------ | ---- | -------------- | ------------
 * 0    | 0    | 0    | 0    | Normal             | 0xFF | Immediate      | MAX
 * 1    | 0    | 0    | 1    | PWM, Phase Correct | 0xFF | TOP            | BOTTOM
 * 2    | 0    | 1    | 0    | CTC                | OCRA | Immediate      | MAX
 * 3    | 0    | 1    | 1    | Fast PWM           | 0xFF | TOP            | MAX
 * 4    | 1    | 0    | 0    | Reserved           | –    | –              | –
 * 5    | 1    | 0    | 1    | PWM, Phase Correct | OCRA | TOP            | BOTTOM
 * 6    | 1    | 1    | 0    | Reserved           | –    | –              | –
 * 7    | 1    | 1    | 1    | Fast PWM           | OCRA | BOTTOM         | TOP
 * Note:
 *   1. MAX    = 0xFF
 *   2. BOTTOM = 0x00
 *
 *
 * ### Clock Select Bit Description (Register TCCR0B)
 *
 * CS02 | CS01 | CS00 | Description
 * 0    | 0    | 0    | No clock source (Timer/Counter stopped)
 * 0    | 0    | 1    | clk I/O /1    (No prescaling)
 * 0    | 1    | 0    | clk I/O /8    (From prescaler)
 * 0    | 1    | 1    | clk I/O /64   (From prescaler)
 * 1    | 0    | 0    | clk I/O /256  (From prescaler)
 * 1    | 0    | 1    | clk I/O /1024 (From prescaler)
 * 1    | 1    | 0    | External clock source on T0 pin. Clock on falling edge.
 * 1    | 1    | 1    | External clock source on T0 pin. Clock on rising  edge.
 */
/** Initialize Interrupt.
 *
 * ### Init INT2 (PD2) - External Interrupt Request
 *
 * Documentation: Atmel 2549L–AVR–08/07, Chapter 15: External Interrupts (Page 112)
 *
 * - EICRA –> Set to: Falling edge of INT2 generates asynchronly an interrupt request
 * - EIMSK -> Enable 'External Interrupt Mask' for INT2
 *
 *
 * ### Init TIMER0 (8 Bit Timer used to scale from 16MHz to 1KHz).
 *
 * Cherry Key Switches are specificated with a debounce delay of 5ms.
 *
 *
 * F_TIMER0 = F_CPU / (OCR0A * prescaler)
 *   1ms :   1      KHz = 16MHz / (250 *   64)
 *   4ms : 250       Hz = 16MHz / (250 *  256)
 *  >5ms : 195.3125  Hz = 16MHz / ( 80 * 1024)
 *
 * - TCCR0A -> Set Mode 'Clear Timer on Compare Match' (CTC)
 * - TCCR0B -> Set 'Clocksource From Prescaler' to 64
 * - OCR0A  -> Set 'Output Compare Register A'  to 250
 * - TIMSK0 -> Enable 'Output Compare Match A' interrupt
 */
KeyEvent::KeyEvent(void)
{
	// Init External Interrupt Request (from TWI): INT2
	EICRA = (1<<ISC21) | (0<<ISC20);
	EIMSK = (1<<INT2);
}


bool
KeyEvent::isNewKeyEventAvailable(void)
{
	return Flag_NewKeyEventAvailable;
}


void
KeyEvent::clearNewKeyEventAvailable(void)
{
	Flag_NewKeyEventAvailable = false;
}


DebounceTimer::DebounceTimer()
{
	// Init Timer Interrupt (debounce timer - 5ms <=> 200Hz): TIMER0
	TCCR0A = (1<<WGM01);
	TCCR0B = (1<<CS02) | (0<<CS01) | (0<<CS00);
	OCR0A  = 200;
}


void
DebounceTimer::startTimer(void)
{
	TIMSK0 = (1<<OCIE0A);
}


void
DebounceTimer::stopTimer(void)
{
	TIMSK0 = (0<<OCIE0A);
}


bool
DebounceTimer::isTimeUp(void)
{
	return Flag_DebounceTimerTimeUp;
}


void
DebounceTimer::clearTimeUp(void)
{
	Flag_DebounceTimerTimeUp = false;
}


PrintTimer::PrintTimer(void)
{
	// Init Timer3 (16-bit) in Clear Timer on Compare (CTC) Mode with Interrupt
	// for TESTing
	TCCR3A = (1<<WGM32);                        // mode
	TCCR3B = (1<<CS32) | (0<<CS31) | (0<<CS30); // prescaler  256
//	TCCR3B = (1<<CS32) | (0<<CS31) | (1<<CS30); // prescaler 1024
	OCR3A  = 15625;                             // output compare register
	TIMSK3 = (1<<OCIE3A);                       // enable interrupt A
}


bool
PrintTimer::isTimeUp(void)
{
	return Flag_PrintTimerTimeUp;
}


void
PrintTimer::clearTimeUp(void)
{
	Flag_PrintTimerTimeUp = false;
}

