/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Scheduler Modul.
///
/// @file scheduler.cc
/// @author Martin Singer


#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>

#include "scheduler.h"


/** Default Constructor. */
Scheduler::Scheduler()
:
	scancodes(),
	input(&scancodes),
	output(&scancodes)
{}


/** Inititates the sleep (standby) if no input or output events are pending.
 *
 * For an example about sleep mode have a look at included header file:
 *
 * * /usr/avr/include/avr/sleep.h
 *
 */
void
Scheduler::sleep(void)
{
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	if (input.isReadyForSleep() == true &&
	    output.isReadyForSleep() == true)
	{
		cli();
		sleep_enable();
		sei();
		sleep_cpu();
		sleep_disable();
	}
	sei();
}


/** The scheduler loop.
 *
 * Calls the input and output modules.
 */
void
Scheduler::run(void)
{
	while (1) {
		sleep();
		input.call();
		output.call();
	}
}

