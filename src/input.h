/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt.
///
/// @file input.h
/// @author Martin Singer


#ifndef INPUT_H
#define INPUT_H

#include <avr/io.h>
#include "interrupt.h"
#include "scancode.h"
#include "kmx.h"


class Input {
	public:
		explicit Input(Scancode*);
		void call(void);

		bool isReadyForSleep(void);

	protected:

		void checkKeyEvent(void);
		void checkDebounceTimer(void);

		KeyEvent key_event;
		DebounceTimer debounce_timer;

		Scancode *scancodes;

		KMX key_matrix_left;
		KMX key_matrix_right;
};


#endif // INPUT_H

