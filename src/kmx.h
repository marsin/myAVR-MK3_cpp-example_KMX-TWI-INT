/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt.
///
/// @file kmx.h
/// @author Martin Singer


#ifndef KMX_H
#define KMX_H


#include "twi/port_extension.h"
#include "interrupt.h"
#include "scancode.h"


/** 8x8 Key Matrix (KMX).
 *
 *	  7 o----x-x-x-x-x-x-x-x
 *	         | | | | | | | |
 *	  6 o----x-x-x-x-x-x-x-x
 *	         | | | | | | | |
 *	  5 o----x-x-x-x-x-x-x-x
 *	         | | | | | | | |
 *	  4 o----x-x-x-x-x-x-x-x
 *	         | | | | | | | |
 *	^ 3 o----x-x-x-x-x-x-x-x
 *	|        | | | | | | | |
 *	| 2 o----x-x-x-x-x-x-x-x
 *	         | | | | | | | |
 *	r 1 o----x-x-x-x-x-x-x-x
 *	o        | | | | | | | |
 *	w 0 o----x-x-x-x-x-x-x-x
 *	         | | | | | | | |
 *	         | | | | | | | |
 *	         o o o o o o o o
 *	         0 1 2 3 4 5 6 7
 *	         col -->
 */
class KMX {
	public:
		explicit KMX();
		explicit KMX(uint8_t, uint8_t, DebounceTimer*, Scancode*, uint8_t);

		void pollForStage(void);
		void pollForState(void);

	protected:
		TWI::PortExtension TwiWriterIC,
		                   TwiReaderIC;
		uint8_t KeyStateColumns[8],
		        KeyStageColumns[8];

		DebounceTimer *debounce;
		Scancode *Scancodes;
		uint8_t Side;
};


#endif // KMX_H

