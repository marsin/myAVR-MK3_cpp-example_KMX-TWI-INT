/******************************************************************************
 * myAVR-MK3_cpp-driver
 * ====================
 *
 *   - C++ driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_cpp-driver.
 *
 *    MyAVR-MK3_cpp-driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3_cpp-driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3_cpp-driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

/// Driver for the LCD on the MyAVR MK3 development board.
///
/// @file lcd.cc
/// @author Martin Singer


#include <util/delay_basic.h> // for _dealy_loop_2()

#include "board.h"
#include "lcd.h"

#define MK3_LCD_PCP_PS  7 ///< LCD Pin of Control Port - P/S
#define MK3_LCD_PCP_MI  6
#define MK3_LCD_PCP_L   5
#define MK3_LCD_PCP_E   4
#define MK3_LCD_PCP_RW  3
#define MK3_LCD_PCP_RS  2
#define MK3_LCD_PCP_RST 1
#define MK3_LCD_PCP_CS  0

using namespace MK3;


/** Default ASCII font array.
 *
 * 5x7px ASCII characters 0x20..0x7F (32..127)
 */
volatile const uint8_t Font5x7[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00,// (space) ascii minus 0x20
	0x00, 0x00, 0x5F, 0x00, 0x00,// !
	0x00, 0x07, 0x00, 0x07, 0x00,// "
	0x14, 0x7F, 0x14, 0x7F, 0x14,// #
	0x24, 0x2A, 0x7F, 0x2A, 0x12,// $
	0x23, 0x13, 0x08, 0x64, 0x62,// %
	0x36, 0x49, 0x55, 0x22, 0x50,// &
	0x00, 0x05, 0x03, 0x00, 0x00,// '
	0x00, 0x1C, 0x22, 0x41, 0x00,// (
	0x00, 0x41, 0x22, 0x1C, 0x00,// )
	0x08, 0x2A, 0x1C, 0x2A, 0x08,// *
	0x08, 0x08, 0x3E, 0x08, 0x08,// +
	0x00, 0x50, 0x30, 0x00, 0x00,// ,
	0x08, 0x08, 0x08, 0x08, 0x08,// -
	0x00, 0x60, 0x60, 0x00, 0x00,// .
	0x20, 0x10, 0x08, 0x04, 0x02,// /
	0x3E, 0x51, 0x49, 0x45, 0x3E,// 0
	0x00, 0x42, 0x7F, 0x40, 0x00,// 1
	0x42, 0x61, 0x51, 0x49, 0x46,// 2
	0x21, 0x41, 0x45, 0x4B, 0x31,// 3
	0x18, 0x14, 0x12, 0x7F, 0x10,// 4
	0x27, 0x45, 0x45, 0x45, 0x39,// 5
	0x3C, 0x4A, 0x49, 0x49, 0x30,// 6
	0x01, 0x71, 0x09, 0x05, 0x03,// 7
	0x36, 0x49, 0x49, 0x49, 0x36,// 8
	0x06, 0x49, 0x49, 0x29, 0x1E,// 9
	0x00, 0x36, 0x36, 0x00, 0x00,// :
	0x00, 0x56, 0x36, 0x00, 0x00,// ;
	0x00, 0x08, 0x14, 0x22, 0x41,// <
	0x14, 0x14, 0x14, 0x14, 0x14,// =
	0x41, 0x22, 0x14, 0x08, 0x00,// >
	0x02, 0x01, 0x51, 0x09, 0x06,// ?
	0x32, 0x49, 0x79, 0x41, 0x3E,// @
	0x7E, 0x11, 0x11, 0x11, 0x7E,// A
	0x7F, 0x49, 0x49, 0x49, 0x36,// B
	0x3E, 0x41, 0x41, 0x41, 0x22,// C
	0x7F, 0x41, 0x41, 0x22, 0x1C,// D
	0x7F, 0x49, 0x49, 0x49, 0x41,// E
	0x7F, 0x09, 0x09, 0x01, 0x01,// F
	0x3E, 0x41, 0x41, 0x51, 0x32,// G
	0x7F, 0x08, 0x08, 0x08, 0x7F,// H
	0x00, 0x41, 0x7F, 0x41, 0x00,// I
	0x20, 0x40, 0x41, 0x3F, 0x01,// J
	0x7F, 0x08, 0x14, 0x22, 0x41,// K
	0x7F, 0x40, 0x40, 0x40, 0x40,// L
	0x7F, 0x02, 0x04, 0x02, 0x7F,// M
	0x7F, 0x04, 0x08, 0x10, 0x7F,// N
	0x3E, 0x41, 0x41, 0x41, 0x3E,// O
	0x7F, 0x09, 0x09, 0x09, 0x06,// P
	0x3E, 0x41, 0x51, 0x21, 0x5E,// Q
	0x7F, 0x09, 0x19, 0x29, 0x46,// R
	0x46, 0x49, 0x49, 0x49, 0x31,// S
	0x01, 0x01, 0x7F, 0x01, 0x01,// T
	0x3F, 0x40, 0x40, 0x40, 0x3F,// U
	0x1F, 0x20, 0x40, 0x20, 0x1F,// V
	0x7F, 0x20, 0x18, 0x20, 0x7F,// W
	0x63, 0x14, 0x08, 0x14, 0x63,// X
	0x03, 0x04, 0x78, 0x04, 0x03,// Y
	0x61, 0x51, 0x49, 0x45, 0x43,// Z
	0x00, 0x00, 0x7F, 0x41, 0x41,// [
	0x02, 0x04, 0x08, 0x10, 0x20,// "\"
	0x41, 0x41, 0x7F, 0x00, 0x00,// ]
	0x04, 0x02, 0x01, 0x02, 0x04,// ^
	0x40, 0x40, 0x40, 0x40, 0x40,// _
	0x00, 0x01, 0x02, 0x04, 0x00,// `
	0x20, 0x54, 0x54, 0x54, 0x78,// a
	0x7F, 0x48, 0x44, 0x44, 0x38,// b
	0x38, 0x44, 0x44, 0x44, 0x20,// c
	0x38, 0x44, 0x44, 0x48, 0x7F,// d
	0x38, 0x54, 0x54, 0x54, 0x18,// e
	0x08, 0x7E, 0x09, 0x01, 0x02,// f
	0x08, 0x14, 0x54, 0x54, 0x3C,// g
	0x7F, 0x08, 0x04, 0x04, 0x78,// h
	0x00, 0x44, 0x7D, 0x40, 0x00,// i
	0x20, 0x40, 0x44, 0x3D, 0x00,// j
	0x00, 0x7F, 0x10, 0x28, 0x44,// k
	0x00, 0x41, 0x7F, 0x40, 0x00,// l
	0x7C, 0x04, 0x18, 0x04, 0x78,// m
	0x7C, 0x08, 0x04, 0x04, 0x78,// n
	0x38, 0x44, 0x44, 0x44, 0x38,// o
	0x7C, 0x14, 0x14, 0x14, 0x08,// p
	0x08, 0x14, 0x14, 0x18, 0x7C,// q
	0x7C, 0x08, 0x04, 0x04, 0x08,// r
	0x48, 0x54, 0x54, 0x54, 0x20,// s
	0x04, 0x3F, 0x44, 0x40, 0x20,// t
	0x3C, 0x40, 0x40, 0x20, 0x7C,// u
	0x1C, 0x20, 0x40, 0x20, 0x1C,// v
	0x3C, 0x40, 0x30, 0x40, 0x3C,// w
	0x44, 0x28, 0x10, 0x28, 0x44,// x
	0x0C, 0x50, 0x50, 0x50, 0x3C,// y
	0x44, 0x64, 0x54, 0x4C, 0x44,// z
	0x00, 0x08, 0x36, 0x41, 0x00,// {
	0x00, 0x00, 0x7F, 0x00, 0x00,// |
	0x00, 0x41, 0x36, 0x08, 0x00,// }
	0x08, 0x08, 0x2A, 0x1C, 0x08,// ->
	0x08, 0x1C, 0x2A, 0x08, 0x08 // <-
};


// Driver functions

/** Initilaizes the control and data DDR and PORT registers for the LCD and the LCD. */
Lcd::Lcd()
{
	// LCD data (PortC)
	MK3_LCD_DATA_DDR  = 0x00; // input (changes input/output in dependence of action)
	MK3_LCD_DATA_PORT = 0x00; // disable pull-up resistors (data out)

	// LCD control (PortA)
	MK3_LCD_CTRL_DDR  = 0xFF;      // output
	MK3_LCD_CTRL_PORT = 0b1100011; // pre init

	// Init
	_delay_loop_2(60000);                      // wait 3.75ms (clock: 16MHz)
	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_L);  // LOW:  light off
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_CS); // HIGH: chip select off
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_PS); // HIGH: set prallel mode
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_MI); // HIGH: set 6800 mode

	// Reset
	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_RST); // LOW:  reset
	_delay_loop_2(10000);                       // wait 0.625ms (clock: 16MHz)
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_RST); // HIGH: run
	_delay_loop_2(10000);                       // wait 0.625ms (clock: 16MHz)

	// Send control commands to LCD
	// (read datasheet for details)
	sendCommandByte(0xE2); // internal reset
	sendCommandByte(0xA0); // adc select
	sendCommandByte(0xC8); // COM output scan direction = reverse
	sendCommandByte(0xA2); // LCD drive voltage bias ratio
	sendCommandByte(0x2F); // ???
	sendCommandByte(0x26); // ???
	sendCommandByte(0xF8); // select booster ratio
	sendCommandByte(0x00); // ???
	sendCommandByte(0x81); // set output voltage
	sendCommandByte(0x09); // ???
	sendCommandByte(0xE0); // read-modify-write
	sendCommandByte(0xAF); // display on

	Font = Font5x7;
}


/** Turn LCD backlight on or off.
 *
 * @param on (false := OFF; true := ON)
 */
void
Lcd::turnLightOn(const bool on = true)
{
	if (on == true) {
		MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_L); // HIGH: light on
	} else {
		MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_L); // LOW:  light off
	}
}


/** Basic send byte to LCD routine.
 *
 * Used by:
 * * sendCommandByte()
 * * sendDataByte()
 *
 * @param byte
 */
void
Lcd::sendByte(const uint8_t byte)
{
	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_RW); // LOW:  write

	MK3_LCD_DATA_PORT  = byte;                 // copy data to data port
	MK3_LCD_DATA_DDR   = 0xFF;                 // config data port as ouput

	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_CS); // LOW:  chip select on
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_E);  // L->H: write edge

	asm volatile ("nop \n nop \n nop \n");     // wait

	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_E);  // HIGH: reset write edge
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_CS); // LOW:  chip select off
}


/** Send command byte (control data) to LCD.
 *
 * @param c command byte
 */
void
Lcd::sendCommandByte(const uint8_t command_byte)
{
	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_RS); // LOW: control data
	sendByte(command_byte);
}


/** Send data byte (display data) to LCD
 *
 * @param d data
 *
 * Writes a byte on the actual position at the LCD
 * (actual position will increase by one).
 */
void
Lcd::sendDataByte(const uint8_t data_byte)
{
	MK3_LCD_CTRL_PORT |= (1<<MK3_LCD_PCP_RS); // HIGH: display data
	sendByte(data_byte);
}


/** Read data byte from LCD.
 *
 * Reads the byte from the actual position of the LCD
 * (actual position will not change).
 *
 * @return data byte
 */
uint8_t
Lcd::readDataByte(void)
{
	uint8_t data_byte = 0x00;

	MK3_LCD_DATA_DDR   = 0x00;                 // config data port as input
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_RS); // HIGH: display data
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_RW); // HIGH: read
	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_CS); // LOW:  chip select on

	// Dummy Read after address change (must be!)
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_E);  // HIGH: pre-load read edge
	asm volatile ("nop \n nop \n nop \n");
	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_E);  // H->L: read edge

	// Read Data
	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_E);  // HIGH: pre-load read edge
	asm volatile ("nop \n nop \n nop \n");
//	data_byte = MK3_LCD_DATA_PIN;              // read data (pos like in the example)
	MK3_LCD_CTRL_PORT &= ~(1<<MK3_LCD_PCP_E);  // H->L: read edge
	data_byte = MK3_LCD_DATA_PIN;              // read data

	MK3_LCD_CTRL_PORT |=  (1<<MK3_LCD_PCP_CS); // HIGH: chip select off
	MK3_LCD_DATA_DDR   = 0xFF;                 // config data port as output

	return data_byte;
}



/** Set the position pointer of the LCD (also used for the write cursor position).
 *
 * @param pos_x pixel in x (0..127)
 * @param pos_y pixel in y (0..63, becomes rounded down to 8)
 *
 *
 * ## Quick notes:
 * - x is pixel-orientated
 * - y is  byte-orientated
 *   - This means y-pixel-positions will be converted into byte positions.
 *   - Values will be rounded down to a 8 like value (0, 8, 16, 24, ..., 56)
 *
 *
 * ## About:
 * - The LCD has x=128 x y=64 pixels
 * - Pixels are sent in bytes. Every bit represents one pixel.
 * - Every byte represents a column of 8 verticaly stacked pixels (y).
 * - If one byte is sent, the the position pointer of the LCD steps
 *   on pixel to the right (x) automaticaly.
 * - If a line is full, the position pointer doesn't go to the next line
 *   automaticaly (8 pixels sub).
 * - 64 y-pixels require 8 stacked bytes.
 *
 *
 * ## Read about in the manual:
 * - Page 26: Display Data RAM
 * - Page 50: Table of ST7565R Commands
 */
void
Lcd::setPointerPosition(const uint8_t pos_x,
                        const uint8_t pos_y)
{
	// send the x-pos (pixel position) in two steps
	sendCommandByte(0x10 + (pos_x >> 4));   // first the upper nibble (4 bits)
	sendCommandByte(0x00 + (pos_x & 0x0F)); // then the lower nibble

	// send the y-pos (byte position)
	sendCommandByte(0xB0 + (pos_y >> 3));   // shifts 3 lower bits out (rounds down)
}


/** Moves the inputline to the actual line on the LCD (scrolleffect).
 *
 * @param lines number of lines to go
 */
void
Lcd::shiftToLine(const uint8_t line)
{
	sendCommandByte(0x40 + (line & 0x3F));
}


/** Clears all data on the LCD.
 *
 * (this function is very slow)
 */
void
Lcd::clearDisplay(void)
{
	for (uint8_t y = 0; y < 64; y+=8) {          // for every y-byte
		setPointerPosition(0, y);            // goto line begin
		for (uint16_t x = 0; x < 128; ++x) { // for every x-pixel
			sendDataByte(0x00);          // set every pixel in stacked y-byte
		}
	}
	setPointerPosition(0, 0);
}


/** Inverse display colors.
 *
 * (white to black; black to white)
 *
 *
 * @param inverse
 */
void
Lcd::inverseDisplayColors(const bool inverse = true)
{
	if (inverse == true) {
		sendCommandByte(0xA7); // 0b10100111
	} else {
		sendCommandByte(0xA6); // 0b10100110
	}
}


/** Turn Display ON/OFF.
 *
 * @param on true  := ON
 *           false := OFF (complete dark)
 */
void
Lcd::turnDisplayOn(const bool on = true)
{
	if (on == true) {
		sendCommandByte(0xA4); // ON  - 0b10100100
	} else {
		sendCommandByte(0xA5); // OFF - 0b10100101
	}
}


/** Set a pixel on position x, y at the LCD.
 *
 * @param pos_x [0..127]
 * @param pos_y [0..63]
 */
void
Lcd::drawPixel(const uint8_t pos_x,
               const uint8_t pos_y)
{
	uint8_t pixel_byte = 1 << (pos_y & 0x07);  // mask valid part of pos_y number (like modulo 8)
                                                   // and shift a pixel (the 1) to this position in the byte
	setPointerPosition(pos_x, pos_y);          // (pos_x in pixel, pos_y rounded to values of 8 [byte])
	sendDataByte(readDataByte() | pixel_byte); // get the the byte from LCD and mask the pixel on top
}


/** Print an ASCII character.
 *
 * @param character ASCII character [0x21 ... 0x7E]
 *
 * prints an ASCII character in 5x7 font size
 */
void
//Lcd::print5x7AsciiCharacter(char character)
Lcd::printAsciiCharacter(char character)
{
	volatile const uint8_t* pt_font = nullptr;

	if (Font == nullptr){
		pt_font = Font5x7;
	} else {
		pt_font = Font;
	}

	// Font5x7 defines ASCII characters 0x20-0x7F (32-127)
	if (0x20 > character || 0x7F < character) {
//		return;  // variant1: exit
		character = '.'; // variant2: replace invalid character
	}

	character -= 0x20; // ASCII table 0x20 is in Font5x7 table 0x00

	// for each char shift Font 5 pixels (char width) - is like Font[character * 5]
	while (character--) {
		for (uint8_t i=0x05; i; --i, ++pt_font);
	}

	// print character (5 pixel width + 1 pixel gap)
	for (uint8_t i=0x05; i; --i, ++pt_font) {
		sendDataByte(*pt_font); // send one pixel column
	}
	sendDataByte(0x00);             // send one pixel gap
}


void
Lcd::setFont(volatile const uint8_t* font)
{
	if (font != nullptr) {
		Font = font;
	}
}

