/******************************************************************************
 * myAVR-MK3_cpp-driver
 * ====================
 *
 *   - C++ driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_cpp-driver.
 *
 *    MyAVR-MK3_cpp-driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3_cpp-driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3_cpp-driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Driver for the Seven Segment on the MyAVR MK3 development board.
///
/// @file   seven_segment.h
/// @author Martin Singer


#ifndef MK3_SEVEN_SEGMENT_H
#define MK3_SEVEN_SEGMENT_H


namespace MK3 {

class SevenSegment {
	public:
		explicit SevenSegment();

		void writePort(const uint8_t);
		volatile uint8_t* readPort(void);

		void clearSegments(void);
		void showDot(const uint8_t);
		void printDigit(const uint8_t);
		void printCharacter(const char);
	
	protected:
		static const uint8_t Characters[]; ///< Bitcombinations for character signs.
		const uint8_t DEFAULT_PATTERN = 0xFF; ///< The default/error output pattern.
		const uint8_t DEFAULT_INDEX = 0xFF;   ///< The default (not used) array index.
};

} // namespace MK3


#endif // MK3_SEVEN_SEGMENT_H

