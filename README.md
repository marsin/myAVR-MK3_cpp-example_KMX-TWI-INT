myAVR-MK3_KMX-TWI-INT
=====================

  myAVR-MK3 board with Key Matrix (KMX) on TWI (I2C) bus with Interrrupt


Firmware for testing a key matrix (KMX), using two port extension ICs,
connected by the Two Wire Interface (TWI / I2C) and an external interrupt
to the microcontroller.
Test board is the MyAVR MK3 development board."

This program is written in C++.

Copyright (c) 2017 Martin Singer <martin.singer@web.de>
Licensed under the terms of the GNU GPLv3+.


About
-----

This test program waits for a key interrupt of the key matrix (KMX),
initiated by the reader TWI port expander IC.
If the external key interrupt form the TWI reader IC is received,
the firmware polls all keys to register all key states
to compare this states with a map of the key states.
Is a changing granted, the firmware starts a timer
for awaiting the debounce time of the key.
Is the time up, all keys becomes polled again,
to check up the new state is still there.
If the state is still there, the key becomoes pushed to an event stack
and the state map becomes updated.


Configuration
-------------

* mySmartUSB MK3 programmer
* myAVR MK3 board
  - Port-D0: TWI SCL
  - Port-D1: TWI SDA
  - Port-D2: TWI /INT
* Breadboard with TWI port extension ICs
* Key switch matrix board (KMX)

![Image: Configuration](img/configuration_photo.jpg "Configuration")


Creating the Doxygen reference
------------------------------

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html

Creates a directory "doc" with the source documentation,
created from the doxygen comments inside the source files
and the README.md file.


Compiling and Programming
-------------------------

Install the `avr-gcc` (compiler) and the `avrdude` (programmer).
Have a look at the Makefile and adjust it to your belongs.
Possibly change the programmer.


	$ cd src
	$ make clean
	$ make
	$ make program


Circuits
--------

### The Board

[board]: http://shop.mymcu.de/Systemboards%20und%20Programmer/myAVR%20Board%20MK3%20256K%20PLUS.htm?sp=article.sp.php&artID=100063 "myAVR Board MK3 256K PLUS"
[programmer]: http://shop.mymcu.de/systemboards%20and%20programmer/mySmartUSB%20MK3%20(Programmer%20und%20Bridge).htm?sp=article.sp.php&artID=100058 "mySmartUSB MK3"
[mcu]: http://www.atmel.com/devices/ATMEGA2560.aspx "ATmega2560"

* Board: [myAVR Board MK3 256K PLUS][board]
* Programmer: [mySmartUSB MK3][programmer]
* MCU: [Atmel ATmega2560, 16MHz][mcu]


### The Breadbord with the TWI ICs

The photos and schematics show the board prepared for using by interrupt mode.


#### TWI ICs: PCF8574AP

- Datasheet: <http://www.nxp.com/documents/data_sheet/PCF8574.pdf>
- The address byte looks like __[0|1|1|1|A2|A1|A0|R/W]__ (_Page  9_)
- The maximal operation frequency is 100kHz              (_Page 16_)
- The I/Os should be HIGH before being used as inputs    (_Page 12_)
- This IC has a 100uA current driver
- The pullup resistors R1 and R2 for SCL and SDA have 22K ohms

![Image: Breadboard schematic](img/breadboard_schematic.png "Breadboard schematic")

![Image: Breadboard photo](img/breadboard_photo.jpg "Breadboard photo")

### The Key switch matrix board (KMX)

![Image: KMX schematic](img/kmx_schematic.png "KMX schematic")

